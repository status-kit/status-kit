<p align="center"><img src="./app/assets/StatusKit-light.png" width="50%"></p>

StatusKit é um sistema de página de status de código aberto simples e poderoso. Que oferece integrações para diferentes serviços conhecidos e uma poderosa API para realizar todas as operações necessárias.

Você administra um site ou aplicativo? Tem um ou mais sistemas dos quais seus clientes/comunidade dependem? se algo der errado e seu site estiver fora do ar, achamos que o mais importante é que você consiga se comunicar de forma clara e fácil com seus usuários: seja transparente sobre o problema, o que você faz para resolvê-lo e quando você voltará ou voltará conectados.

É chamada de página de status e deve funcionar independentemente do que aconteça com seu site e aplicativo. E deve ser simples. E de preferência grátis. Criamos StatusKit para fazer exatamente isso!

## Overview

- Liste seus componentes de serviço
- Temas variados
- Uma poderosa API JSON
- Integração nativa com Zabbix

## Como funciona

Primeiramente devemos informar que o **StatusKit** é uma página de status que não faz checagem dos serviços cadastrados, é possível fazer essa mudança de status diretamente por meio da pela de admin do [Django](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/) mas desencorajamos essa alternativa. Para usufruir do potencial desse sistema, disponibilizamos uma API para as operações necessárias, ou seja, criação de componentes, atualização de status, atualização de campos, entre outros, um CRUD completo.

O **StatusKit** também implementa uma ferramenta que cadastra componentes na aplicação com base em um grupo de *hosts* no [Zabbix]() e atualiza seu status com base nas *triggers* de monitoramento dos hosts.

## Requisitos

Você pode executar a aplicação em qualquer uma dessas plataformas:

* Máquina Windows ou Linux.
* Docker
* Kubernetes

Você pode executar a partir do Python `3.4.*` ou superior.

## Instalação

### development

Para executar a aplicação na sua máquina para teste com o Python recomendamos a utilização de ambientes virtuais, caso esteja em um Linux, encorajamos o uso do [pyenv](https://github.com/pyenv/pyenv).

## Primeiros passos

## Configuração

## Deployment

## Costumização
