import os
import pprint

from urllib import parse

import click
import requests

from rich import print
from statuskit.zabbix import Zabbix

pp = pprint.PrettyPrinter(indent=2, compact=True, width=70)


@click.group()
def cli():
    pass


@cli.command('list-groups', help='list all groups in Zabbix.')
@click.option('--zabbix-url', help='Zabbix api url')
@click.option('-u', '--zabbix-user', help='user in Zabbix')
@click.option(
    '-p', '--zabbix-password', help='user in Zabbix', prompt=True, hide_input=True,
    default=lambda: os.environ.get("ZABBIX_USER", ""))
def cli_list_groups(zabbix_url, zabbix_user, zabbix_password):
    zb = Zabbix(zabbix_url, zabbix_user, zabbix_password)
    pp.pprint(zb.groups())


@cli.command('list-hosts', help='list all hosts in group.')
@click.argument('group-id', type=int)
@click.option('--zabbix-url', help='Zabbix api url')
@click.option('-u', '--zabbix-user', help='user in Zabbix')
@click.option(
    '-p', '--zabbix-password', help='user in Zabbix', prompt=True, hide_input=True,
    default=lambda: os.environ.get("ZABBIX_USER", ""))
def cli_list_groups(group_id, zabbix_url, zabbix_user, zabbix_password):
    zb = Zabbix(zabbix_url, zabbix_user, zabbix_password)
    hosts = zb.hosts_by_group(group_id)
    pp.pprint(hosts.get('result'))


@cli.command('create', help='create components in Cachet from group Zabbix.')
@click.argument('group-id', type=int)
@click.option('--zabbix-url', help='Zabbix api url')
@click.option('--zabbix-user', help='user in Zabbix', default=lambda: os.environ.get("ZABBIX_USER", ""))
@click.option(
    '-p', '--zabbix-password', help='user in Zabbix', prompt=True, hide_input=True,
    default=lambda: os.environ.get("ZABBIX_PASSWORD", ""))
@click.option('-u', '--statuskit-url', help='Statuskit api url', default='http://localhost:8000/api/components/')
def cli_create(group_id, zabbix_url, zabbix_user, zabbix_password, statuskit_url):
    zb = Zabbix(zabbix_url, zabbix_user, zabbix_password)
    hosts = zb.hosts_by_group(group_id).get('result')

    for host in hosts:
        url_parse = parse.urlsplit(zabbix_url)
        host_zabbix = url_parse.hostname
        host_protocol = url_parse.scheme
        req = requests.post(
            statuskit_url,
            json={
                "name": host.get('host'),
                "hostname": host.get('host'),
                "display_name": host.get('name'),
                "description": host.get('description'),
                "tags": "zabbix",
                "url": f"{host_protocol}://{host_zabbix}/hostinventories.php?hostid=%s" % host.get('hostid'),
                "enabled": True,
                "status": 2
            })
        if req.status_code == 201:
            print(
                ":heavy_check_mark:",
                "[blue]create new component[/blue] '[bold green]%s[/bold green]'" % host.get('name')
            )
        else:
            print(
                "[bold red]Oops... ocorreu um erro na criação do component '%s' no StatusKit![/bold red]" \
                % host.get('name')
            )


if __name__ == '__main__':
    cli()
