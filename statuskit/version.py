author_info = (
    ("Paulo Roberto", "paulo.rb.beserra@gmail.com"),
)

project_home = ""

package_info = ""
package_license = "MIT"

team_email = ""

version_info = (0, 1)

__author__ = ", ".join("{} <{}>".format(*info) for info in author_info)
__version__ = ".".join(map(str, version_info))
