import sys
from loguru import logger

config = {
    "handlers": [
        {"sink": sys.stdout, "level": "DEBUG", "backtrace": True, "diagnose": True},
        {"sink": "statuskit.log", "level": "INFO", "backtrace": True, "diagnose": True},
    ]
}

# logger.add('statuskit.log', level='INFO', backtrace=True, diagnose=True)
logger.configure(**config)
