from django.contrib import admin
from .models import Component


@admin.register(Component)
class ComponentsList(admin.ModelAdmin):
    list_display = ('id', 'name', 'hostname', 'url', 'enabled', 'status')
    search_fields = ('name', 'hostname', 'display_name')
    list_filter = ('enabled', 'status')
    list_display_links = ('name',)
    list_per_page = 100
