from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_index, name='index'),
    path('status', views.view_apple, name='apple')
]
