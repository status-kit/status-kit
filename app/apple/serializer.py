from .models import Component
from rest_framework import serializers, viewsets


class ComponentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Component
        fields = [
            'id',
            'name',
            'hostname',
            'display_name',
            'description',
            'tags',
            'url',
            'enabled',
            'status'
        ]


class ComponentViewSet(viewsets.ModelViewSet):
    queryset = Component.objects.all()
    serializer_class = ComponentSerializer
